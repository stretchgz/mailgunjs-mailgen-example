# Get started

Example of using [Mailgen](https://github.com/eladnava/mailgen) and [Mailgun-js](https://github.com/bojand/mailgun-js).

1. Clone this repo
2. Install dependencies `npm install`
3. Edit config file: `config/default.json` or create local dev config: `config/local.json`
4. Configure
5. Have fun

# License

* MIT
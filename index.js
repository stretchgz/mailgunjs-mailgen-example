const Config = require('getconfig');
const fs = require('fs');
const Mailgen = require('mailgen');

/**
 * configure Mailgun-js
 */
if (!Config.mailgun.apiKey || !Config.mailgun.domain) {
  throw new Error('Missing apikey or domain, check your config file.');
}

const mailgun = require('mailgun-js')({
  apiKey: Config.mailgun.apiKey,
  domain: Config.mailgun.domain
});

/**
 * configure Mailgen
 */
const mailGenerator = new Mailgen({
  theme: 'default',
  product: {
    name: 'cool-website-name',
    link: 'https://example.com'
  }
});
const email = {
  body: {
    name: 'user123',
    intro: 'You have received this email because a password reset request for your account was received.',
    action: {
      instructions: 'Click the button below to reset your password: ',
      button: {
        color: '#D50000',
        text: 'Reset your password',
        link: 'https://example.com/reset?query=blahblahblah'
      }
    },
    outro: [
      'If you did not request a password reset...blahblahblah.',
      'Please do not reply this email directly. Email sent to this address is not monitored.'
    ]
  }
};
const emailBody = mailGenerator.generate(email);
const emailText = mailGenerator.generatePlaintext(email);

fs.writeFileSync('preview.html', emailBody, 'utf8');
console.log(emailText);

// send email
const data = {
  from: 'Site Support <support@example.com>',
  to: '', // your@email.com
  subject: 'Hello blahblahblah',
  html: emailBody.toString('ascii'),
  text: emailText
};

mailgun.messages().send(data)
  .then(body => {
    console.log(body);
  })
  .catch(err => {
    console.error(err);
  });
